CREATE DATABASE portfolio;
USE portfolio;

CREATE TABLE user(
    userName VARCHAR(50) NOT NULL,
    userPassword VARCHAR(60) NOT NULL
);
CREATE TABLE about(
    title VARCHAR(50) NOT NULL,
    userDescription VARCHAR(250) NOT NULL,
    userResume VARCHAR(500) NOT NULL
);
CREATE TABLE softSkills(
    softSkill VARCHAR(50) NOT NULL
);
CREATE TABLE technologies(
    technology VARCHAR(50) NOT NULL,
    levelTechnology int(1) NOT NULL
);
CREATE TABLE experience(
    titleExperience VARCHAR(50) NOT NULL,
    experienceDescription VARCHAR(250) NOT NULL
);
CREATE TABLE contact(
    contactName VARCHAR(50) NOT NULL,
    email VARCHAR(70) NOT NULL,
    contactMessage(500) VARCHAR NOT NULL
)