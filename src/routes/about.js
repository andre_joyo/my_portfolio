const express = require('express');
const router = express.Router();

const pool = require('../database');
router.post('/add', async (req,res) => {
    console.log(req.body);
    const { title, description } = req.body;
    const about = {
        title,
        description
    };
    console.log(about);
    await pool.query('INSERT INTO about SET ?', [about]);
    res.send('recibido');
});
router.get('/', async (req,res) => {
    const about = await pool.query('SELECT * FROM about');
    console.log(about);
    res.render('about/list', { about: about[0] }) //res.send('Tencologías irán aquí');
});
router.get('/edit', async (req,res) => {
    const {about} = req.params;
    const aboutReturned = await pool.query('SELECT title, userDescription FROM about');
    res.render('about/edit', {about: aboutReturned[0]});
});
router.post('/edit/:title', async (req,res) => {
    const { title, userDescription } = req.body;
    const newAbout = {
        title,
        userDescription
    }; //console.log(newAbout); 
    await pool.query('UPDATE about SET ?', [newAbout]);
    res.redirect('/about'); //res.send('recibido');
});
module.exports = router;