const express = require('express');
const router = express.Router();

const pool = require('../database');

router.get('/', async (req,res) => {
    res.render('contact/formContact');
});
router.post('/contacting', async (req,res) => {
    console.log(req.body);
    const { contactName, email, contactMessage } = req.body;
    const newContact = {
        contactName,
        email,
        contactMessage
    };
    console.log(newContact);
    await pool.query('INSERT INTO contact SET ?', [newContact]);
    res.redirect('/contact'); //res.send('recibido');

});
module.exports = router;