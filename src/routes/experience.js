const express = require('express');
const router = express.Router();

const pool = require('../database');
router.get('/add', (req,res) => {
    res.render('experience/add');  //res.send('form');
});
router.post('/add', async (req,res) => {
    console.log(req.body);
    const { titleExperience, experienceDescription } = req.body;
    const newExperience = {
        titleExperience,
        experienceDescription
    }; //console.log(newExperience);
    await pool.query('INSERT INTO experience SET ?', [newExperience]);
    res.redirect('/experience'); //res.send('recibido');
});
router.get('/', async (req,res) => {
    const experience = await pool.query('SELECT * FROM experience'); //console.log(experience);
    res.render('experience/list', { experience }) //res.send('Tencologías irán aquí');
});
router.get('/delete/:title', async (req,res) => {
    const {title} = req.params;
    await pool.query('DELETE FROM experience WHERE titleExperience=?', [title]);
    res.redirect('/experience');
});
router.get('/edit/:title', async (req,res) => {
    const {title} = req.params;
    const experience = await pool.query('SELECT titleExperience, experienceDescription FROM experience WHERE titleExperience=?', [title]);
    res.render('experience/edit', {experience: experience[0]});
});
router.post('/edit/:title', async (req,res) => {
    const { title, titleExperience, experienceDescription } = req.body;
    const newExperience = {
        titleExperience,
        experienceDescription
    }; //console.log(newExperience); 
    await pool.query('UPDATE experience SET ? WHERE titleExperience=?', [newExperience, title]);
    res.redirect('/experience'); //res.send('recibido');
});
module.exports = router;