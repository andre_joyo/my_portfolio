const express = require('express');
const router = express.Router();

const pool = require('../database');
router.get('/add', (req,res) => {
    res.render('softSkills/add');  //res.send('form');
});
router.post('/add', async (req,res) => {
    console.log(req.body);
    const { softSkill, levelSoftSkill } = req.body;
    const newSoftSkill = {
        softSkill,
        levelSoftSkill
    };
    console.log(newSoftSkill);
    await pool.query('INSERT INTO softSkills SET ?', [newSoftSkill]);
    res.redirect('/softSkills'); //res.send('recibido');
});
router.get('/', async (req,res) => {
    const softSkills = await pool.query('SELECT * FROM softskills');
    console.log(softSkills);
    res.render('softSkills/list', { softSkills }) //res.send('Tencologías irán aquí');
});
router.get('/delete/:softSkill', async (req,res) => {
    const {softSkill} = req.params;
    await pool.query('DELETE FROM softskills WHERE softskill=?', [softSkill]);
    res.redirect('/softskills');
});
router.get('/edit/:softSkill', async (req,res) => {
    const {softSkill} = req.params;
    const softSkillReturned = await pool.query('SELECT softSkill, levelSoftSkill FROM softskills WHERE softSkill=?', [softSkill]);
    res.render('softSkills/edit', {currentSoftSkill: softSkillReturned[0]});
});
router.post('/edit/:softSkill', async (req,res) => {
    const { currentSoftSkill, softSkill, levelSoftSkill } = req.body;
    const newSoftSkill = {
        softSkill,
        levelSoftSkill
    }; //console.log(newSoftSkill); 
    await pool.query('UPDATE softSkills SET ? WHERE softSkill=?', [newSoftSkill, currentSoftSkill]);
    res.redirect('/softSkills'); //res.send('recibido');
});
module.exports = router;