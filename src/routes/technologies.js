const express = require('express');
const router = express.Router();

const pool = require('../database');
router.get('/add', (req,res) => {
    res.render('technologies/add');  //res.send('form');
});
router.post('/add', async (req,res) => {
    console.log(req.body);
    const { technology, levelTechnology } = req.body;
    const newTechnology = {
        technology,
        levelTechnology
    };
    console.log(newTechnology);
    await pool.query('INSERT INTO technologies SET ?', [newTechnology]);
    res.redirect('/technologies'); //res.send('recibido');
});
router.get('/', async (req,res) => {
    const technologies = await pool.query('SELECT * FROM technologies');
    console.log(technologies);
    res.render('technologies/list', { technologies }) //res.send('Tencologías irán aquí');
});
router.get('/delete/:technology', async (req,res) => {
    //console.log(req.params.technology);res.send('eliminado');
    const {technology} = req.params;
    await pool.query('DELETE FROM technologies WHERE technology=?', [technology]);
    res.redirect('/technologies');
});
router.get('/edit/:technology', async (req,res) => {
    const {technology} = req.params;
    const technologyReturned = await pool.query('SELECT technology, levelTechnology FROM technologies WHERE technology=?', [technology]);
    res.render('technologies/edit', {currentTechnology: technologyReturned[0]});
});
router.post('/edit/:technology', async (req,res) => {
    const { currentTechnology, technology, levelTechnology } = req.body;
    const newTechnology = {
        technology,
        levelTechnology
    }; //console.log(newTechnology); console.log('CURRENT TECHNOLOGY: ', {currentTechnology});
    await pool.query('UPDATE technologies SET ? WHERE technology=?', [newTechnology, currentTechnology]);
    res.redirect('/technologies'); //res.send('recibido');
});
module.exports = router;